package com.example.kingshuk.polyglot.ViewPagerOptions.Message.Chat_Screen;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.kingshuk.polyglot.Constants;
import com.example.kingshuk.polyglot.OtherActivities.Miscellaneous.BlockPojo;
import com.example.kingshuk.polyglot.OtherActivities.Profiles.UserPojo;
import com.example.kingshuk.polyglot.R;
import com.example.kingshuk.polyglot.ViewPagerOptions.Home.RecyclerItemClickListener;
import com.example.kingshuk.polyglot.ViewPagerOptions.Message.ChatPojo;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.memetix.mst.language.Language;
import com.memetix.mst.translate.Translate;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;



/**
 * Created by SAPTARSHI DE on 05-07-2016.
 */
public class ChatPage extends AppCompatActivity {
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    FirebaseDatabase database;
    FirebaseUser use;
    DatabaseReference lastOnlineRef;
    DatabaseReference connectedRef;
    DatabaseReference typing;
    DatabaseReference block,block1;
    String mlistId="";
    TextView mUsername,mUserLast_Seen,translate;
    EditText mUserText;
    ImageView chatProfilePic;
    private List<ChatMessagePojo> homeList = new ArrayList<ChatMessagePojo>();
    private RecyclerView recyclerView;
    FirebaseRecyclerAdapter<ChatMessagePojo, ChatHolder> mRecyclerViewAdapter;
    LinearLayoutManager mLayoutManager;
    String month[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    String weekday[] = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
    int days[]={31,28,31,30,31,30,31,31,30,31,30,31};
    String text,translatedText;
    String blocked="";
    String name_typing,m;
    Menu menu;
    MenuItem bedMenuItem;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.MyToolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Intent intent = this.getIntent();
        mlistId = intent.getStringExtra(Constants.KEY_ACTIVE_CHAT);
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = preferences.edit();
        editor.putString("CHAT_PAGE_OPENED", mlistId);
        editor.apply();

        database = FirebaseDatabase.getInstance();
        use = FirebaseAuth.getInstance().getCurrentUser();
        lastOnlineRef = database.getReference(Constants.LAST_ONLINE).child(use.getUid());
        connectedRef = database.getReference(Constants.CONNECTIONS).child(use.getUid());
        typing = database.getReference(Constants.TYPING).child(use.getUid()).child(mlistId);
      /*  if((use.getUid().compareTo(mlistId))>=0)   //comparing strings
            node_string=use.getUid()+mlistId;
        else
        node_string=mlistId+use.getUid();*/
        block=database.getReference(Constants.BLOCK).child(use.getUid()).child(mlistId);
        block1=database.getReference(Constants.BLOCK).child(mlistId).child(use.getUid());
        typing.setValue(Boolean.FALSE);
        connectedRef.setValue(Boolean.TRUE);
        //blocked=block.getKey();
        chatProfilePic = (ImageView) findViewById(R.id.chatProfilePic);
        mUserText = (EditText) findViewById(R.id.chat_talk);
        mUsername = (TextView) findViewById(R.id.chat_name);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_chat);
        mUserLast_Seen = (TextView) findViewById(R.id.last_seen);


        connectedRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                boolean connected = snapshot.getValue(Boolean.class);
                if (connected) {
                    // add this device to my connections list
                    // this value could contain info about the device or a timestamp too
                    connectedRef.setValue(Boolean.TRUE);

                    // when this device disconnects, remove it
                    connectedRef.onDisconnect().setValue(Boolean.FALSE);
                    typing.onDisconnect().setValue(Boolean.FALSE);
                    // when I disconnect, update the last time I was seen online
                    lastOnlineRef.onDisconnect().setValue(ServerValue.TIMESTAMP);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }


        });



        // getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
        mUserText.addTextChangedListener(new TextWatcher() {


            //  boolean typingStarted=false;
            public void afterTextChanged(Editable s) {
                if ((mUserText.getText().toString()).trim().length() == 0) {
                    typing.setValue(Boolean.FALSE);
                } else {
                    typing.setValue(Boolean.TRUE);
                }

            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

            }
        });

        final DatabaseReference myRef = database.getReference(Constants.USERS).child(mlistId);
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final UserPojo userPojo = dataSnapshot.getValue(UserPojo.class);
                mUsername.setText(userPojo.getName());
                name_typing = userPojo.getName();
                Picasso.with(ChatPage.this).load(Uri.parse(userPojo.getImage())).networkPolicy(NetworkPolicy.OFFLINE)
                        //.resize(field.getHeight(), field.getWidth())
                        .placeholder(chatProfilePic.getDrawable())
                        //.centerCrop()
                        .into(chatProfilePic, new Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError() {
                                Picasso.with(ChatPage.this).load(Uri.parse(userPojo.getImage())).resize(chatProfilePic.getHeight(), chatProfilePic.getWidth()).placeholder(chatProfilePic.getDrawable()).error(chatProfilePic.getDrawable()).centerCrop().into(chatProfilePic);
                            }
                        });

                prepareEntryData();

                //mLayoutManager.setReverseLayout(true);
                recyclerView = (RecyclerView) findViewById(R.id.recycler_view_chat);
                mLayoutManager = new LinearLayoutManager(getApplicationContext());
                mLayoutManager.setStackFromEnd(true);
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setAdapter(mRecyclerViewAdapter);
                // recyclerView.smoothScrollToPosition(recyclerView.getAdapter().getItemCount());

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        DatabaseReference myRef7 = database.getReference(Constants.CONNECTIONS).child(mlistId);
        myRef7.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {

                Boolean presence = (Boolean) dataSnapshot.getValue();
                if (presence == Boolean.TRUE) {
                    mUserLast_Seen.setText("Online");

                    DatabaseReference typingStatus = database.getReference(Constants.TYPING).child(mlistId).child(use.getUid());
                    typingStatus.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            Boolean isTyping = (Boolean) dataSnapshot.getValue();
                            if (isTyping == Boolean.TRUE) {
                                mUserLast_Seen.setText("typing..");
                            } else {
                                mUserLast_Seen.setText("Online");
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });


                } else {
                    DatabaseReference lastOnline = database.getReference(Constants.LAST_ONLINE).child(mlistId);
                    lastOnline.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            Long timestamp = (Long) dataSnapshot.getValue();


                            Calendar calendar = Calendar.getInstance();
                            TimeZone tz = TimeZone.getDefault();
                            calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.getTimeInMillis()));
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            java.util.Date currentTimeZone = new java.util.Date(timestamp);
                            mUserLast_Seen.setText(sdf.format(currentTimeZone));
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
            }


            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        ImageView send = (ImageView) findViewById(R.id.send_button);
        //ImageView chatProfilePic = (ImageView) findViewById(R.id.chatProfilePic);
        //Picasso.with(this).load(R.drawable.p1).into(chatProfilePic);

        if (send != null)//&&!(mUserText.getText().toString()).equals(""))
        {
            send.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if ((mUserText.getText().toString()).trim().length() == 0) {
                    } else {


                        DatabaseReference myRef1 = database.getReference(Constants.CHAT_DETAILS);
                        Calendar now = Calendar.getInstance();
                        // String timestamp = "" + now.get(Calendar.HOUR_OF_DAY) + now.get(Calendar.MINUTE) +
                        //       now.get(Calendar.SECOND) + now.get(Calendar.MILLISECOND);

                        ChatMessagePojo user_details = new ChatMessagePojo(mUserText.getText().toString().trim(), use.getUid());
                        DatabaseReference myRef2 = myRef1.child(
                                use.getUid().toString()).child(mlistId);
                        myRef2.push().setValue(user_details);
                        DatabaseReference myRef3 = myRef1.child(mlistId).child(use.getUid().toString());
                        myRef3.push().setValue(user_details);
                        // prepareEntryData();
                        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_chat);
                        mLayoutManager = new LinearLayoutManager(getApplicationContext());
                        mLayoutManager.setStackFromEnd(true);
                        //mLayoutManager.setReverseLayout(true);
                        recyclerView.setLayoutManager(mLayoutManager);


                        mRecyclerViewAdapter.notifyDataSetChanged();
                        mRecyclerViewAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
                            @Override
                            public void onItemRangeInserted(int positionStart, int itemCount) {
                                super.onItemRangeInserted(positionStart, itemCount);
                                int friendlyMessageCount = mRecyclerViewAdapter.getItemCount();
                                int lastVisiblePosition = mLayoutManager.findLastCompletelyVisibleItemPosition();
                                // If the recycler view is initially being loaded or the user is at the bottom of the list, scroll
                                // to the bottom of the list to show the newly added message.
                                if (lastVisiblePosition == -1 ||
                                        (positionStart >= (friendlyMessageCount - 1) && lastVisiblePosition == (positionStart - 1))) {
                                    recyclerView.scrollToPosition(positionStart);
                                }
                            }
                        });
                        // recyclerView.setAdapter(mRecyclerViewAdapter);

                        mUserText.setText("");

                    }
                }
            });

        }
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(ChatPage.this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        text = mRecyclerViewAdapter.getItem(position).getMessage();

                        AlertDialog.Builder builder = new AlertDialog.Builder(ChatPage.this);
                        LayoutInflater inflater = ChatPage.this.getLayoutInflater();

                        View v_iew = inflater.inflate(R.layout.dialog_fragment, null);
                        builder.setView(v_iew);
                        translate = (TextView) v_iew.findViewById(R.id.translated_text);
                        new MyAsyncTask() {
                            protected void onPostExecute(Boolean result) {
                                translate.setText(translatedText);
                            }
                        }.execute();

                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // User clicked OK button
                            }
                        });
                        builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // User cancelled the dialog
                            }
                        });


                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                }));


        block.addValueEventListener(new ValueEventListener() {  //listener attached
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChild("block")) {

                    BlockPojo blockPojo = dataSnapshot.getValue(BlockPojo.class);
                    blocked = blockPojo.getBlock().toString();
                    //  Toast.makeText(ChatPage.this, "123" + blocked, Toast.LENGTH_SHORT).show();
                    LinearLayout linearLayout=(LinearLayout)findViewById(R.id.linear_chat);

                    if((""+blocked).equals("BLOCKED")){
                        bedMenuItem.setTitle("Unblock");
                        linearLayout.setVisibility(View.INVISIBLE);
                        mUserLast_Seen.setText("");


                    }
                    else {
                        linearLayout.setVisibility(View.VISIBLE);
                    }


                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



    }

    @Override
    protected void onResume() {
        super.onResume();

        editor.putString("CHAT_PAGE_OPENED",mlistId);
        editor.apply();
    }

    @Override
    public void onUserLeaveHint() {

        typing.setValue(Boolean.FALSE);
        //database.goOffline();//
        connectedRef = database.getReference(Constants.CONNECTIONS).child(use.getUid());
        connectedRef.setValue(Boolean.FALSE);
        lastOnlineRef = database.getReference(Constants.LAST_ONLINE).child(use.getUid());
        lastOnlineRef.setValue(ServerValue.TIMESTAMP);
        editor.putString("CHAT_PAGE_OPENED","closed");
        editor.apply();
        //startService(new Intent(this, MessageService.class));
    }


    public int leap(int y){
        return 7;
    }

    @Override
    public void onStart() {
        super.onStart();

        editor.putString("CHAT_PAGE_OPENED",mlistId);
        editor.apply();
        database.goOnline();

        connectedRef.setValue(Boolean.TRUE);
        //stopService(new Intent(this, MessageService.class));
    }
    void prepareEntryData(){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(Constants.CHAT_DETAILS);
        FirebaseUser use= FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference myRef1=myRef.child(use.getUid()).child(mlistId);
        mRecyclerViewAdapter = new FirebaseRecyclerAdapter<ChatMessagePojo, ChatHolder>(ChatMessagePojo.class, R.layout.chat_message, ChatHolder.class, myRef1) {
            @Override
            public void populateViewHolder(ChatHolder chatMessageViewHolder, ChatMessagePojo chatMessage, int position) {
                //  FirebaseUser use = FirebaseAuth.getInstance().getCurrentUser();
                // String userId = use.getUid();
                // String confirm = mRecyclerViewAdapter.getRef(position).getKey();
                // if (!confirm.equals(userId)) {
                chatMessageViewHolder.setMessage(" "+chatMessage.getMessage()+" ");
                chatMessageViewHolder.setOwner(chatMessage.getOwner());
            }
        };


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_chat, menu);
        this.menu=menu;
        bedMenuItem = menu.findItem(R.id.action_block);
        bedMenuItem.setTitle("Block");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if(id==R.id.action_block){
            block();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStop() {
        //startService(new Intent(this, MessageService.class));
        super.onStop();
        editor.putString("CHAT_PAGE_OPENED","closed");
        editor.apply();
        typing.setValue(Boolean.FALSE);

    }


    void block() // function added
    {

        BlockPojo blockPojo;
        database = FirebaseDatabase.getInstance();
        use = FirebaseAuth.getInstance().getCurrentUser();
        block=database.getReference(Constants.BLOCK).child(use.getUid()).child(mlistId);
        block1=database.getReference(Constants.BLOCK).child(mlistId).child(use.getUid());
        if (blocked.equals("BLOCKED")){
            blockPojo=new BlockPojo("UNBLOCKED");bedMenuItem.setTitle("Block");}
        else {
            blockPojo=new BlockPojo("BLOCKED");
            bedMenuItem.setTitle("Unblock");
        }
        block.setValue(blockPojo);
        block1.setValue(blockPojo);
    }



    class MyAsyncTask extends AsyncTask<Void, Integer, Boolean> {
        @Override
        protected Boolean doInBackground(Void... arg0) {
            Translate.setClientId("MicrosoftTranslatorJavaAPI");
            Translate.setClientSecret("0VHbhXQnJrZ7OwVqcoX/PDZlyLJS9co3cVev1TPr8iM=");
            try {
                translatedText = Translate.execute(text, Language.ENGLISH, Language.SPANISH);
            } catch(Exception e) {
                translatedText = e.toString();
            }


            return true;
        }
    }
}


