package com.example.kingshuk.polyglot.OtherActivities.Profiles;


import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.example.kingshuk.polyglot.Constants;
import com.example.kingshuk.polyglot.MainActivity;
import com.example.kingshuk.polyglot.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import java.util.ArrayList;


public class YourProfile extends AppCompatActivity {
    SharedPreferences.Editor editor;
    SharedPreferences preferences;
    ArrayList<String> interestsSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_your_profile);
        Intent intent=getIntent();
        final String hugeImage=intent.getStringExtra("HUGE_IMAGE");
        interestsSelected=new ArrayList<String>();





        final EditText mUserMoreInfo=(EditText)findViewById(R.id.user_moreinfo);
        final EditText mUserMovies=(EditText)findViewById(R.id.user_movies);
        final EditText mUserSongs=(EditText)findViewById(R.id.user_songs);
        final EditText mUserBooks=(EditText)findViewById(R.id.user_books);
        final TextView mUserInterests=(TextView)findViewById(R.id.user_interests);

        mUserInterests.setClickable(true);
        mUserInterests.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                final String[] INTERESTS = new String[] {
                        "Music", "Football", "Movies", "TV Shows", "Gaming", "Flash", "Arrow", "Coding","Studying"
                };
                // arraylist to keep the selected items
                final ArrayList seletedItems=new ArrayList();

                AlertDialog dialog = new AlertDialog.Builder(YourProfile.this)
                        .setTitle("What Do You Like?")
                        .setMultiChoiceItems(INTERESTS, null, new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int indexSelected, boolean isChecked) {
                                if (isChecked) {
                                    // If the user checked the item, add it to the selected items
                                    seletedItems.add(indexSelected);
                                    interestsSelected.add(INTERESTS[indexSelected]);
                                } else if (seletedItems.contains(indexSelected)) {
                                    // Else, if the item is already in the array, remove it
                                    seletedItems.remove(Integer.valueOf(indexSelected));
                                    interestsSelected.remove(String.valueOf(INTERESTS[indexSelected]));
                                }
                            }
                        }).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                    //  Your code when user clicked on OK
                                    //  You can write the code  to save the selected item here

                                String s="";
                                for(int i=0;i<interestsSelected.size();i++)
                                {
                                    s=s+interestsSelected.get(i)+", ";
                                }
                                mUserInterests.setText(s);
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                //  Your code when user clicked on Cancel
                            }
                        }).create();
                dialog.show();

            }
        });



        LinearLayout saveDetails=(LinearLayout)findViewById(R.id.save_details);
        saveDetails.setClickable(true);
        saveDetails.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                FirebaseUser use=FirebaseAuth.getInstance().getCurrentUser();
                String userId=use.getUid();
                FirebaseDatabase database = FirebaseDatabase.getInstance();




                DatabaseReference myRef1 = database.getReference(Constants.USER_DETAILS);
                UserDetailsPojo user_details=new UserDetailsPojo(mUserMoreInfo.getText().toString(),mUserMovies.getText().toString(),mUserSongs.getText().toString(),mUserBooks.getText().toString(),interestsSelected,hugeImage);
                myRef1.child(use.getUid().toString()).setValue(user_details);


                startActivity(new Intent(getApplicationContext(), MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                overridePendingTransition(R.anim.enter, R.anim.exit);

            }
        });
    }


}

