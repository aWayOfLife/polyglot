package com.example.kingshuk.polyglot.OtherActivities.Miscellaneous;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Kingshuk on 10/8/2016.
 */
public class CountryManager {
    String json = null;
    Context mContext;
    DatabaseHelper myDb;

    public String loadJSONFromAsset(Context context) {

        try {
            mContext=context;
            InputStream is = context.getAssets().open("country_data.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public void GenerateCountryList(Context context)
    {
        try {
            JSONObject obj = new JSONObject(loadJSONFromAsset(context));
            JSONArray m_jArry = obj.getJSONArray("countries");


            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);
                String country_value = jo_inside.getString("Country_Name");
                String continent_value = jo_inside.getString("Continent_Code");

                //Insert the two values into sqlite database

                //Add your values in your `ArrayList` as below:
                //m_li = new HashMap<String, String>();
                //m_li.put("formule", formula_value);
                //m_li.put("url", url_value);

                //formList.add(m_li);
                myDb = new DatabaseHelper(mContext);
                myDb.insertdata(country_value,continent_value);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
