package com.example.kingshuk.polyglot.OtherActivities.Profiles;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kingshuk on 29-Jun-16.
 */
public class UserPojo {
    String name,age,tag,introduction,userId,gender,country,image;
    ArrayList<String> languages;
    public UserPojo() {
    }

    public UserPojo(String name, String age, String tag, String introduction,String userId,String gender,String country, ArrayList<String> languages,String image) {
        this.name = name;
        this.age = age;
        this.tag = tag;
        this.introduction = introduction;
        this.userId=userId;
        this.gender=gender;
        this.country=country;
        this.languages=languages;
        this.image=image;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }


    public String getName() {
        return name;
    }

    public String getIntroduction() {
        return introduction;
    }

    public String getTag() {
        return tag;
    }

    public String getAge() {
        return age;
    }

    public String getGender(){return gender;}

    public String getCountry(){return country;}

    public ArrayList<String> getLanguages() {
        return languages;
    }

    public void setLanguages(ArrayList<String> languages) {
        this.languages = languages;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public void setGender(String gender){this.gender=gender;}

    public void setCountry(String country){this.country=country;}

}
