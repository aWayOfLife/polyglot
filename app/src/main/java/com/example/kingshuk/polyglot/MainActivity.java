package com.example.kingshuk.polyglot;




import android.app.ActivityManager;
import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.kingshuk.polyglot.OtherActivities.Login_Signup.LoginActivity;
import com.example.kingshuk.polyglot.OtherActivities.Miscellaneous.Filter;
import com.example.kingshuk.polyglot.OtherActivities.Miscellaneous.MyViewPager;
import com.example.kingshuk.polyglot.OtherActivities.Miscellaneous.SearchActivity;
import com.example.kingshuk.polyglot.Services.MessageService;
import com.example.kingshuk.polyglot.ViewPagerOptions.Account.Account;
import com.example.kingshuk.polyglot.ViewPagerOptions.Friends.Friends;
import com.example.kingshuk.polyglot.ViewPagerOptions.Home.Home;
import com.example.kingshuk.polyglot.ViewPagerOptions.Message.Message;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity{
    protected FirebaseAuth.AuthStateListener mAuthListener;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    FirebaseDatabase database;
    FirebaseUser use;
    DatabaseReference lastOnlineRef;
    DatabaseReference connectedRef;
    ViewPager viewPager;
    MenuItem searchItem;
    SearchView searchView;
    String search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = preferences.edit();
        String state = preferences.getString("STATE","");
        editor.putString("MAIN EXIT TYPE", "Main Activity");
        editor.apply();
        if(state.equalsIgnoreCase("Signed In")==false)
        {
            takeUserToLoginScreenOnUnAuth();
        }
        setContentView(R.layout.activity_main);
        viewPager = (ViewPager) findViewById(R.id.pager);
        initializeScreen();
        if(isMyServiceRunning(MessageService.class)==false) {
            startService(new Intent(this, MessageService.class));
        }

    }
    @Override
    public void onStart() {

        editor.putString("MAIN EXIT TYPE", "Main Activity");
        editor.apply();
        //stopService(new Intent(this, MessageService.class));


        super.onStart();
        database = FirebaseDatabase.getInstance();
        use = FirebaseAuth.getInstance().getCurrentUser();
        lastOnlineRef = database.getReference(Constants.LAST_ONLINE).child(use.getUid());
        connectedRef = database.getReference(Constants.CONNECTIONS).child(use.getUid());
        lastOnlineRef.keepSynced(true);
        connectedRef.keepSynced(true);
        connectedRef.setValue(Boolean.TRUE);


        connectedRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                boolean connected = snapshot.getValue(Boolean.class);
                if (connected) {
                    // add this device to my connections list
                    // this value could contain info about the device or a timestamp too
                    connectedRef.setValue(Boolean.TRUE);

                    // when this device disconnects, remove it
                    connectedRef.onDisconnect().setValue(Boolean.FALSE);

                    // when I disconnect, update the last time I was seen online
                    lastOnlineRef.onDisconnect().setValue(ServerValue.TIMESTAMP);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }


        });
        database.goOnline();

    }
    @Override
    public void onResume() {

        editor.putString("MAIN EXIT TYPE", "Main Activity");
        editor.apply();
        //stopService(new Intent(this, MessageService.class));


        super.onResume();
        database.goOnline();
        connectedRef.setValue(Boolean.TRUE);
    }


    private void takeUserToLoginScreenOnUnAuth() {
        /* Move user to LoginActivity, and remove the backstack */
        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }



    /**
     * Link layout elements from XML and setup the toolbar
     */
    public void initializeScreen()
    {


        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        //getSupportActionBar().setDisplayUseLogoEnabled(true);
        /**
         * Create SectionPagerAdapter, set it as adapter to viewPager with setOffscreenPageLimit(2)
         **/
        SectionPagerAdapter adapter = new SectionPagerAdapter(getSupportFragmentManager());
        viewPager.setOffscreenPageLimit(2);

        viewPager.setAdapter(adapter);
        /**
         * Setup the mTabLayout with view pager
         */
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(0).setIcon(R.drawable.ic_home_black_24dp);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_message_black_24dp);
        tabLayout.getTabAt(2).setIcon(R.drawable.ic_menu_black_24dp);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                invalidateOptionsMenu();
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }




   @Override
    public void onDestroy() {
       //startService(new Intent(this, MessageService.class));
        super.onDestroy();
        try {
          // database.goOffline();
            connectedRef = database.getReference(Constants.CONNECTIONS).child(use.getUid());
            connectedRef.setValue(Boolean.FALSE);

        }
        catch(Exception e){}
    }
    @Override
    public void onUserLeaveHint() {

        String exit=preferences.getString("MAIN EXIT TYPE","");
        if(exit.equalsIgnoreCase("Main Activity")) {
            try {


                //database.goOffline();//

                connectedRef = database.getReference(Constants.CONNECTIONS).child(use.getUid());
                connectedRef.setValue(Boolean.FALSE);
               // startService(new Intent(this, MessageService.class));

            } catch (Exception e) {
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //startService(new Intent(this, MessageService.class));
        String exit=preferences.getString("MAIN EXIT TYPE","");
        if(exit.equalsIgnoreCase("Main Activity")) {
            try {

               //database.goOffline();
                connectedRef = database.getReference(Constants.CONNECTIONS).child(use.getUid());
                connectedRef.setValue(Boolean.FALSE);

            } catch (Exception e) {
            }
        }
    }

    /**
     * SectionPagerAdapter class that extends FragmentStatePagerAdapter to save fragments state
     */
    public class SectionPagerAdapter extends FragmentStatePagerAdapter {

        public SectionPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        /**
         * Use positions (0 and 1) to find and instantiate fragments with newInstance()
         *
         * @param position
         */
        @Override
        public Fragment getItem(int position) {

            Fragment fragment = null;

            /**
             * Set fragment to different fragments depending on position in ViewPager
             */
            switch (position) {
                case 0:
                    fragment = Home.newInstance();
                    break;
                case 1:
                    fragment = Message.newInstance();
                    break;
                case 2:
                    fragment = Account.newInstance();
                    break;
                default:
                    fragment = Home.newInstance();
                    break;
            }

            return fragment;
        }


        @Override
        public int getCount() {
            return 3;
        }


    }
    @Override
    public void onPause(){

        super.onPause();

    }

    @Override
    public void onStop() {

        super.onStop();

    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        for(int i = 0; i < menu.size(); i++){
            Drawable drawable = menu.getItem(i).getIcon();
            if(drawable != null) {
                drawable.mutate();
                drawable.setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
            }
        }

        if (viewPager.getCurrentItem()==0){
            menu.findItem(R.id.action_search).setVisible(true);
            menu.findItem(R.id.action_filter).setVisible(true);
        } else if(viewPager.getCurrentItem()==1){
            menu.findItem(R.id.action_filter).setVisible(false);
            menu.findItem(R.id.action_search).setVisible(false);
        } else if(viewPager.getCurrentItem()==2){
            menu.findItem(R.id.action_filter).setVisible(false);
            menu.findItem(R.id.action_search).setVisible(false);
            // configure
        } else if(viewPager.getCurrentItem()==3){
            menu.findItem(R.id.action_filter).setVisible(false);
            menu.findItem(R.id.action_search).setVisible(false);
            // configure
        }

        /*if(menu.findItem(R.id.action_search).isVisible()==true)
        {
            SearchManager searchManager =
                    (SearchManager) getSystemService(Context.SEARCH_SERVICE);
            SearchView searchView =
                    (SearchView) menu.findItem(R.id.action_search).getActionView();
            ComponentName cn = new ComponentName(this, SearchActivity.class);
            searchView.setSearchableInfo(searchManager.getSearchableInfo(cn));

        }*/





        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if(id == R.id.action_filter)
        {
            Intent intent=new Intent(this, Filter.class);
            editor.putString("MAIN EXIT TYPE","Other Activity");
            editor.apply();
            startActivity(intent);
        }
        if(id == R.id.action_search)
        {
            Intent intent=new Intent(this, SearchActivity.class);
            editor.putString("MAIN EXIT TYPE","Other Activity");
            editor.apply();
            startActivity(intent);
        }




        return super.onOptionsItemSelected(item);
    }
    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}



