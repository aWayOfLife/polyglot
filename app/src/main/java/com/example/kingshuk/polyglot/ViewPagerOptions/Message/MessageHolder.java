package com.example.kingshuk.polyglot.ViewPagerOptions.Message;

import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.kingshuk.polyglot.Constants;
import com.example.kingshuk.polyglot.R;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

/**
 * Created by SAPTARSHI DE on 05-07-2016.
 */
public class MessageHolder extends RecyclerView.ViewHolder {
    View mView;
    ImageView imageView;
    public MessageHolder(View itemView) {
        super(itemView);
        mView = itemView;
    }

    public void setName(String name) {
        TextView field = (TextView) mView.findViewById(R.id.items_message_name);
        field.setText(name);
    }
    public void setUserId(String text) {
       final ImageView field=(ImageView)mView.findViewById(R.id.item_message_ProfilePic);
        //Picasso.with(mView.getContext()).load(R.drawable.p1).into(chatProfilePic);
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReferenceFromUrl("gs://polyglot-2-126ec.appspot.com");
        storageRef.child("ProfilePic/" + text + ".jpg").getDownloadUrl().addOnSuccessListener(
                new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        // Got the download URL for 'users/me/profile.png'
                        Picasso.with(mView.getContext()).load(uri).resize(field.getHeight(),field.getWidth()).centerCrop().into(field);
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle any errors
            }
        });


        imageView=(ImageView)mView.findViewById(R.id.online_green);
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef7 = database.getReference(Constants.CONNECTIONS).child(text);
        myRef7.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
               Boolean status=(Boolean)dataSnapshot.getValue();
                if(status==Boolean.TRUE) {
                    imageView.setColorFilter(Color.argb(255, 0, 255, 0));}
                    else{//combo for grey

                    imageView.setColorFilter(Color.rgb(128, 128, 128));
                    }
                }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
}
