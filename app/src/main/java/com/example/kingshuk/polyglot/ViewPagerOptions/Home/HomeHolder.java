package com.example.kingshuk.polyglot.ViewPagerOptions.Home;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.graphics.Color;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kingshuk.polyglot.Constants;
import com.example.kingshuk.polyglot.OtherActivities.Miscellaneous.DatabaseHelper;
import com.example.kingshuk.polyglot.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import me.gujun.android.taggroup.TagGroup;

/**
 * Created by Kingshuk on 02-Jul-16.
 */
public class HomeHolder extends RecyclerView.ViewHolder {
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    String gender, min_age, max_age;
    View mView;
    Context mContext;
    boolean gone;
    int min=0;
    int max=96;
    FirebaseStorage storage = FirebaseStorage.getInstance();
    StorageReference storageRef = storage.getReferenceFromUrl("gs://polyglot-2-126ec.appspot.com");

    public HomeHolder(View itemView) {
        super(itemView);

        mView = itemView;
    }

    public void setName(String name) {
        TextView field = (TextView) mView.findViewById(R.id.home_item_name);
        field.setText(name);
    }

    public void setAge(String text) {

            mContext = mView.getContext();
            preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
            editor = preferences.edit();
            min_age = preferences.getString("FILTER_MINAGE", "0");
            max_age = preferences.getString("FILTER_MAXAGE", "96");
            min= Integer.parseInt(min_age);
            max= Integer.parseInt(max_age);
            //code that may throw exception






        TextView field = (TextView) mView.findViewById(R.id.home_item_age);
        Long r = Long.parseLong(text) * 1000;
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(r);
        Calendar calendar1 = Calendar.getInstance();
        int age1 = 0;
        String age = "" + (calendar1.get(Calendar.YEAR) - calendar.get(Calendar.YEAR));
        if (calendar1.get(Calendar.YEAR) != calendar.get(Calendar.YEAR))
        {
            if ((calendar1.get(Calendar.MONTH)) < ((calendar.get(Calendar.MONTH))))
            {
                age1 = Integer.parseInt(age);
                age = "" + (--age1);
            } else if ((calendar1.get(Calendar.MONTH)) == ((calendar.get(Calendar.MONTH)))) {

                if ((calendar1.get(Calendar.DAY_OF_MONTH)) < ((calendar.get(Calendar.DAY_OF_MONTH)))) {
                    age1 = Integer.parseInt(age);
                    age = "" + (--age1);
                }
            }


            if(Integer.parseInt(age)>= min && Integer.parseInt(age)<= max ) {
                field.setText(age);
            }
            else
            {
                gone=true;
                mView.findViewById(R.id.layout_to_hide).setVisibility(View.GONE);
            }
        }
    }

    public void setTag(String text) {
        TextView field = (TextView) mView.findViewById(R.id.home_item_tag);
        field.setText(text);
    }

    public void setIntroduction(String text) {
        TextView field = (TextView) mView.findViewById(R.id.home_item_introduction);


        field.setText(text);
    }



    public void setUserId(String text) {

        //TAKING CARE OF LIKES
            final TextView likes = (TextView) mView.findViewById(R.id.home_likes);
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference profileLikes = database.getReference(Constants.PROFILE_LIKES).child(text);
            profileLikes.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.getValue() == null || (long) dataSnapshot.getValue() == 0) {
                        likes.setText("0+");
                    } else {
                        likes.setText((long) dataSnapshot.getValue() + "+");
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }




    public void setImage(final String text) {
        //TAKING CARE OF IMAGES
        if (gone == false) {
            final ImageView field = (ImageView) mView.findViewById(R.id.home_item_profile_pic);
            Picasso.with(mView.getContext()).load(Uri.parse(text)).networkPolicy(NetworkPolicy.OFFLINE)
                    //.resize(field.getHeight(), field.getWidth())
                    .placeholder(field.getDrawable())
                    //.centerCrop()
                    .into(field, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError() {
                    Picasso.with(mView.getContext()).load(Uri.parse(text)).resize(field.getHeight(), field.getWidth()).placeholder(field.getDrawable()).error(field.getDrawable()).centerCrop().into(field);
                }
            });


        }
    }

    public void setGender(String text) {

        mContext = mView.getContext();
        preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        editor = preferences.edit();
        gender = preferences.getString("FILTER_GENDER", "");

        ImageView imageView = (ImageView) mView.findViewById(R.id.gender_display);
        if (gender.equals("ALL") || gender.equals(null) || gender.equals("")) {
            if (text.equals("M")) {
                imageView.setImageResource(R.drawable.ic_gender_male_white_18dp);
                imageView.setColorFilter(Color.parseColor("#2196F3"));
            } else {
                imageView.setImageResource(R.drawable.ic_gender_female_white_24dp);
                imageView.setColorFilter(Color.parseColor("#F06292"));
            }
        }

        if (gender.equals("M")) {
            if (text.equals("M")) {
                imageView.setImageResource(R.drawable.ic_gender_male_white_18dp);
                imageView.setColorFilter(Color.parseColor("#2196F3"));
            } else {
                gone=true;
                mView.findViewById(R.id.layout_to_hide).setVisibility(View.GONE);
            }
        }
        if (gender.equals("F")) {
            if (text.equals("F")) {
                imageView.setImageResource(R.drawable.ic_gender_female_white_24dp);
                imageView.setColorFilter(Color.parseColor("#F06292"));
            } else {
                gone=true;
                mView.findViewById(R.id.layout_to_hide).setVisibility(View.GONE);
            }
        }
    }



    public void setCountry(String text) {


        ImageView imageView = (ImageView) mView.findViewById(R.id.country_display);
        String flag = text.substring(0, text.lastIndexOf(","));
        String countryName = text.substring(text.lastIndexOf(",") + 1);
        String zones = preferences.getString("FILTER_ZONES", "Asia.Africa.Europe.North America.South America.Oceania");
        String[] selectedZones = zones.split("\\.");

        DatabaseHelper myDb;
        myDb = new DatabaseHelper(mContext);

        try {

            myDb.createDataBase();

        } catch (IOException ioe) {

            throw new Error("Unable to create database");

        }

        try {

            myDb.openDataBase();

        }catch(SQLException sqle){

            throw sqle;

        }

        Cursor res = myDb.getAllData();
        if (res.getCount() != 0) {
            String countryDB, continentDB;
            while (res.moveToNext()) {
                countryDB = res.getString(1);

                if (countryDB.equalsIgnoreCase(countryName)) {
                    continentDB = res.getString(2);
                    int count = 0;
                    for (int i = 0; i < selectedZones.length; i++) {

                        if (selectedZones[i].equalsIgnoreCase(continentDB)) {
                            count++;

                        }
                    }
                    if (count > 0) {
                        imageView.setImageResource(Integer.parseInt(flag));
                    } else
                    {
                        gone = true;
                        mView.findViewById(R.id.layout_to_hide).setVisibility(View.GONE);
                    }
                    break;
                }
            }
        }
        else
        {
            imageView.setImageResource(Integer.parseInt(flag));
        }
    }






    public void setLanguages(ArrayList<String> languages)
        {
        TagGroup langStrong =(TagGroup)mView.findViewById(R.id.tag_languages_strong);
        TagGroup langWeak =(TagGroup)mView.findViewById(R.id.tag_languages_weak);
        ArrayList<String> strong=new ArrayList<String>();
        ArrayList<String> weak=new ArrayList<String>();
        for (int i=0;i<languages.size();i++)
        {
            int s=languages.get(i).indexOf(" ");
            char c=languages.get(i).charAt(s+1);
            if(c=='F' || c=='N')
            {
                strong.add(languages.get(i).substring(0,s));
            }
            else
            {
                weak.add(languages.get(i).substring(0,s));
            }
        }

        langStrong.setTags(strong);
        langWeak.setTags(weak);
    }
}