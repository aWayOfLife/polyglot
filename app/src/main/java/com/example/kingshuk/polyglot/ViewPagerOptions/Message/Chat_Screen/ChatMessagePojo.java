package com.example.kingshuk.polyglot.ViewPagerOptions.Message.Chat_Screen;

/**
 * Created by SAPTARSHI DE on 05-07-2016.
 */
public class ChatMessagePojo {
    String message,owner;

    public ChatMessagePojo() {
    }

    public ChatMessagePojo(String message,String owner) {
        this.message = message;
        this.owner=owner;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getMessage() {

        return message;
    }
    public String getOwner() {

        return owner;
    }
}
