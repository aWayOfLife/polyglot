package com.example.kingshuk.polyglot.ViewPagerOptions.Message;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.kingshuk.polyglot.Constants;
import com.example.kingshuk.polyglot.R;
import com.example.kingshuk.polyglot.ViewPagerOptions.Home.RecyclerItemClickListener;
import com.example.kingshuk.polyglot.ViewPagerOptions.Message.Chat_Screen.ChatPage;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;


public class Message extends Fragment {
    private List<ChatPojo> homeList = new ArrayList<ChatPojo>();
    private RecyclerView recyclerView;
    FirebaseRecyclerAdapter<ChatPojo,MessageHolder> mRecyclerViewAdapter;
    ImageView imageView;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    public Message() {
        /* Required empty public constructor*/
    }


    // TODO: Rename and change types and number of parameters
    public static Message newInstance() {
        Message fragment = new Message();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View rootView=inflater.inflate(R.layout.fragment_message, container, false);

        prepareEntryData();
        recyclerView = (RecyclerView)rootView.findViewById(R.id.recycler_view_message);
        final RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(mRecyclerViewAdapter);
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        // TODO Handle item click
                        ChatPojo selectedList = mRecyclerViewAdapter.getItem(position);
                        if (selectedList != null) {
                            preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
                            editor = preferences.edit();
                            editor.putString("MAIN EXIT TYPE","Other Activity");
                            editor.apply();
                            Intent intent = new Intent(getContext(), ChatPage.class);
                            String listId = mRecyclerViewAdapter.getRef(position).getKey();
                            intent.putExtra(Constants.KEY_ACTIVE_CHAT, listId);
                            startActivity(intent);
                        }
                    }
                })
        );
        return rootView;
    }


    void prepareEntryData(){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        FirebaseUser use= FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference myRef = database.getReference(Constants.CHAT).child(use.getUid());
        mRecyclerViewAdapter = new FirebaseRecyclerAdapter<ChatPojo, MessageHolder>(ChatPojo.class, R.layout.items_mesage, MessageHolder.class, myRef) {
            @Override
            public void populateViewHolder(MessageHolder chatMessageViewHolder, ChatPojo chatMessage, int position) {
                FirebaseUser use = FirebaseAuth.getInstance().getCurrentUser();
                // String userId = use.getUid();
                // String confirm = mRecyclerViewAdapter.getRef(position).getKey();
                // if (!confirm.equals(userId)) {
                chatMessageViewHolder.setName(chatMessage.getName());
                if (chatMessage.getUserId().equalsIgnoreCase("null") == false)
                    chatMessageViewHolder.setUserId(chatMessage.getUserId());

               /* }

                else{
                   // mRecyclerViewAdapter.getRef(position).removeValue();
                }*/

            }
        }

        ;

    }
    @Override
    public void onPause() {
        super.onPause();
    }
}
