package com.example.kingshuk.polyglot.OtherActivities.Profiles;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kingshuk.polyglot.Constants;
import com.example.kingshuk.polyglot.R;
import com.example.kingshuk.polyglot.ViewPagerOptions.Message.ChatPojo;
import com.example.kingshuk.polyglot.ViewPagerOptions.Message.Chat_Screen.ChatPage;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.ArrayList;
import java.util.Calendar;

import jp.wasabeef.picasso.transformations.BlurTransformation;
import me.gujun.android.taggroup.TagGroup;

public class ProfileActivity extends AppCompatActivity implements AppBarLayout.OnOffsetChangedListener {

    FirebaseDatabase database;
    FirebaseUser use;
    DatabaseReference lastOnlineRef;
    DatabaseReference connectedRef;
    DatabaseReference myLikes;
    DatabaseReference profileLikes;
    ImageView mLike;
    long count;Boolean like;

    String mlistId="";
    TextView mUsername;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Intent intent = this.getIntent();
          mlistId = intent.getStringExtra(Constants.KEY_ACTIVE_LIST);
            if (mlistId == null) {
                finish();
                return;
            }


        Bitmap bitmap = intent.getParcelableExtra("BitmapImage");
        mUsername = (TextView) findViewById(R.id.name);
        final TextView mUserAge = (TextView) findViewById(R.id.age_tag);
        final TextView mUserIntroduction = (TextView) findViewById(R.id.introduction);
        final TextView mUserMoreInfo = (TextView) findViewById(R.id.more_info);
        final TextView mUserMovies = (TextView) findViewById(R.id.movies);
        final TextView mUserSongs = (TextView) findViewById(R.id.songs);
        final TextView mUserBooks = (TextView) findViewById(R.id.books);
        mLike=(ImageView)findViewById(R.id.profileLike);
        final TagGroup mTagGroup = (TagGroup) findViewById(R.id.tag_group);
        final TagGroup langStrong =(TagGroup)findViewById(R.id.profile_languages_strong);
        final TagGroup langWeak =(TagGroup)findViewById(R.id.profile_languages_weak);
        final ImageView blurredPic = (ImageView)findViewById(R.id.blurredPic);
        final ImageView profilePic = (ImageView)findViewById(R.id.profilePic);
        profilePic.setImageBitmap(bitmap);
       //blurredPic.setImageBitmap(bitmap);
        mLike.setClickable(true);


        database = FirebaseDatabase.getInstance();
        use = FirebaseAuth.getInstance().getCurrentUser();
        if (mlistId == null) {
            mlistId=use.getUid();

        }
        lastOnlineRef = database.getReference(Constants.LAST_ONLINE).child(use.getUid());
        connectedRef = database.getReference(Constants.CONNECTIONS).child(use.getUid());
        connectedRef.setValue(Boolean.TRUE);

        myLikes=database.getReference(Constants.MY_LIKES).child(use.getUid()).child(mlistId);
        profileLikes=database.getReference(Constants.PROFILE_LIKES).child(mlistId);


        DatabaseReference myRef = database.getReference(Constants.USERS).child(mlistId);
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                UserPojo userPojo = dataSnapshot.getValue(UserPojo.class);
                mUsername.setText(userPojo.getName());
                Long r=Long.parseLong(userPojo.getAge())*1000;
                Calendar calendar=Calendar.getInstance();
                calendar.setTimeInMillis(r);
                Calendar calendar1=Calendar.getInstance();
                String age=""+(calendar1.get(Calendar.YEAR)-calendar.get(Calendar.YEAR));
                mUserAge.setText(age + "  " + userPojo.getTag());
                mUserIntroduction.setText(userPojo.getIntroduction());
                String flag = userPojo.getCountry();
                flag= flag.substring(0,flag.lastIndexOf(','));
                //mFlag.setImageResource(Integer.parseInt(flag));
                ArrayList<String> languages=userPojo.getLanguages();
                ArrayList<String> strong=new ArrayList<String>();
                ArrayList<String> weak=new ArrayList<String>();
                for (int i=0;i<languages.size();i++)
                {
                    int s=languages.get(i).indexOf(" ");
                    char c=languages.get(i).charAt(s+1);
                    if(c=='F' || c=='N')
                    {
                        strong.add(languages.get(i).substring(0,s));
                    }
                    else
                    {
                        weak.add(languages.get(i).substring(0,s));
                    }
                }

                langStrong.setTags(strong);
                langWeak.setTags(weak);
                String userID=userPojo.getUserId();




            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        DatabaseReference myRef1 = database.getReference(Constants.USER_DETAILS).child(mlistId);
        myRef1.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final UserDetailsPojo  homeItemPojo=dataSnapshot.getValue(UserDetailsPojo.class);
                mUserMoreInfo.setText(homeItemPojo.getMoreInfo());
                mUserMovies.setText(homeItemPojo.getMovies());
                mUserSongs.setText(homeItemPojo.getSongs());
                mUserBooks.setText(homeItemPojo.getBooks());
                if(homeItemPojo.getInterests()!=null)
                mTagGroup.setTags(homeItemPojo.getInterests().toArray(new String[homeItemPojo.getInterests().size()]));
                final Transformation transformation=new BlurTransformation(ProfileActivity.this);
                Picasso.with(ProfileActivity.this).load(Uri.parse(homeItemPojo.getHugeImage())).networkPolicy(NetworkPolicy.OFFLINE).placeholder(profilePic.getDrawable()).into(profilePic, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {
                        Picasso.with(ProfileActivity.this).load(Uri.parse(homeItemPojo.getHugeImage())).placeholder(profilePic.getDrawable()).into(profilePic);
                    }
                });


                Picasso.with(ProfileActivity.this).load(Uri.parse(homeItemPojo.getHugeImage())).networkPolicy(NetworkPolicy.OFFLINE).transform(transformation).into(blurredPic, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {
                        Picasso.with(ProfileActivity.this).load(Uri.parse(homeItemPojo.getHugeImage())).transform(transformation).into(blurredPic);
                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



        final CoordinatorLayout coordinatorLayout = (CoordinatorLayout) findViewById(R.id.main_content);
        final AppBarLayout appBarLayout=(AppBarLayout)findViewById(R.id.MyAppbar);
        Toolbar toolbar=(Toolbar)findViewById(R.id.MyToolbar);
        final NestedScrollView scrollView = (NestedScrollView) findViewById(R.id.scroll);
        final CollapsingToolbarLayout collapsingToolbarLayout=(CollapsingToolbarLayout)findViewById(R.id.collapse_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        appBarLayout.addOnOffsetChangedListener(this);



        // And https://code.google.com/p/android/issues/detail?id=183166
        for (int i = 0; i < appBarLayout.getChildCount(); i++) {
            View childView = appBarLayout.getChildAt(i);
            if (!childView.isClickable()) {
                childView.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        return true;
                    }
                });
            }
        }
        final ImageView imageView = (ImageView) findViewById(R.id.blurredPic);
        final TextView textView=(TextView)findViewById(R.id.name);
        imageView.setLongClickable(true);
        imageView.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                FrameLayout detailsLayout = (FrameLayout) findViewById(R.id.details_primary);
                if(detailsLayout.getAlpha()==1.0f)
                    detailsLayout.animate().alpha(0.0f).setDuration(500);
                else
                    detailsLayout.animate().alpha(1.0f).setDuration(700);
                return true;
            }
        });



        Window window = this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(this.getResources().getColor(R.color.transparent));
        }




        connectedRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                boolean connected = snapshot.getValue(Boolean.class);
                if (connected) {
                    // add this device to my connections list
                    // this value could contain info about the device or a timestamp too
                    connectedRef.setValue(Boolean.TRUE);

                    // when this device disconnects, remove it
                    connectedRef.onDisconnect().setValue(Boolean.FALSE);

                    // when I disconnect, update the last time I was seen online
                    lastOnlineRef.onDisconnect().setValue(ServerValue.TIMESTAMP);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }


        });

        myLikes.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if(dataSnapshot.getValue()== null || (long)dataSnapshot.getValue()==0 )
                {
                    mLike.setColorFilter(Color.parseColor("#FFFFFF"));
                }
                else
                {
                    mLike.setColorFilter(Color.parseColor("#E53935"));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setlikes();
            }
        });


    }


    public void setlikes() {
        myLikes.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(final MutableData currentData) {
                if (currentData.getValue() == null || (long)currentData.getValue() == 0) {

                    currentData.setValue(1);
                    incrementCounter();
                } else {

                    currentData.setValue(0);
                    decrementCounter();
                }

                return Transaction.success(currentData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {

            }


        });
    }


        public void incrementCounter() {
            profileLikes.runTransaction(new Transaction.Handler() {
                @Override
                public Transaction.Result doTransaction(final MutableData currentData) {
                    if (currentData.getValue() == null) {
                        currentData.setValue(1);
                    } else {
                        currentData.setValue((Long) currentData.getValue() + 1);
                    }

                    return Transaction.success(currentData);
                }

                @Override
                public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {

                }


            });
        }
    public void decrementCounter() {
        profileLikes.runTransaction(new Transaction.Handler() {

            @Override
            public Transaction.Result doTransaction(final MutableData currentData) {

                if (currentData.getValue() == null) {
                    currentData.setValue(1);
                } else {
                    currentData.setValue((Long) currentData.getValue() - 1);
                }

                return Transaction.success(currentData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {

            }


        });
    }



    @Override
    public void onStart() {

        database.goOnline();
        connectedRef.setValue(Boolean.TRUE);


        Toast.makeText(ProfileActivity.this, "Start_profile", Toast.LENGTH_SHORT).show();
        super.onStart();

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_profile, menu);
        if(mlistId.equals(use.getUid().toString())){
            menu.findItem(R.id.action_message).setVisible(false);
        }

        return true;
    }
    public String u;
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if(id==R.id.action_message){
            FirebaseUser use= FirebaseAuth.getInstance().getCurrentUser();
            if(mlistId.equals(use.getUid().toString())){

            }
            else{


                DatabaseReference myRef2 = database.getReference(Constants.USERS).child(use.getUid().toString());
                myRef2.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        UserPojo userPojo = dataSnapshot.getValue(UserPojo.class);
                        u = (userPojo.getName());
                        FirebaseDatabase database = FirebaseDatabase.getInstance();
                        DatabaseReference myRef = database.getReference(Constants.CHAT);
                        ChatPojo chatPojo = new ChatPojo(mUsername.getText().toString(), mlistId);
                        FirebaseUser use= FirebaseAuth.getInstance().getCurrentUser();
                        DatabaseReference myref1 = myRef.child(use.getUid().toString());
                        myref1.child(mlistId).setValue(chatPojo);
                        DatabaseReference myref2 = myRef.child(mlistId);
                        ChatPojo chatPojo1 = new ChatPojo(u, use.getUid().toString());
                        myref2.child(use.getUid().toString()).setValue(chatPojo1);

                        Intent intent = new Intent(ProfileActivity.this, ChatPage.class);
                        intent.putExtra(Constants.KEY_ACTIVE_CHAT, mlistId);
                        startActivity(intent);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


                }
            }
                return super.onOptionsItemSelected(item);
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        if (verticalOffset !=-1) {
            FrameLayout detailsLayout = (FrameLayout) findViewById(R.id.details_primary);
            detailsLayout.animate().alpha(1.0f).setDuration(700);

        }
        else
        {
            FrameLayout detailsLayout = (FrameLayout) findViewById(R.id.details_primary);
            // detailsLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onUserLeaveHint() {

        //database.goOffline();
        connectedRef = database.getReference(Constants.CONNECTIONS).child(use.getUid());
        connectedRef.setValue(Boolean.FALSE);
        lastOnlineRef = database.getReference(Constants.LAST_ONLINE).child(use.getUid());
        lastOnlineRef.setValue(ServerValue.TIMESTAMP);
    }
}