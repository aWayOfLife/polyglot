package com.example.kingshuk.polyglot.Services;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import com.example.kingshuk.polyglot.Constants;
import com.example.kingshuk.polyglot.MainActivity;
import com.example.kingshuk.polyglot.OtherActivities.Profiles.UserPojo;
import com.example.kingshuk.polyglot.R;
import com.example.kingshuk.polyglot.ViewPagerOptions.Message.Chat_Screen.ChatMessagePojo;
import com.example.kingshuk.polyglot.ViewPagerOptions.Message.Chat_Screen.ChatPage;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

/**
 * Created by Kingshuk on 7/17/2016.
 */
public class MessageService extends Service {
    @Nullable
    int count = 0;
    int numMessages = 0;
    ArrayList<String> notifications = new ArrayList<String>();
    ArrayList<String> senders = new ArrayList<String>();
    String[] events = new String[10];
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        notifications.clear();
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = preferences.edit();
        Toast.makeText(getBaseContext(), "Service Started", Toast.LENGTH_SHORT).show();
        FirebaseUser use = FirebaseAuth.getInstance().getCurrentUser();
        if(use!=null) {
            final String userId = use.getUid();
            final FirebaseDatabase database = FirebaseDatabase.getInstance();
            database.goOnline();
            final DatabaseReference myRef = database.getReference(Constants.CHAT_DETAILS);
            count = 1;

            myRef.child(userId).addChildEventListener(new ChildEventListener() {

                                                          @Override
                                                          public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                                          }


                                                          @Override
                                                          public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                                                              myRef.child(userId).child(dataSnapshot.getKey()).limitToLast(1).addChildEventListener(new ChildEventListener() {
                                                                  @Override
                                                                  public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                                                      ChatMessagePojo messagePojo = dataSnapshot.getValue(ChatMessagePojo.class);
                                                                      final String owner = messagePojo.getOwner();
                                                                      final String message = messagePojo.getMessage();
                                                                      if (owner.equals(userId) == false) {
                                                                          DatabaseReference userRef = database.getReference(Constants.USERS);
                                                                          userRef.child(owner).addValueEventListener(new ValueEventListener() {
                                                                              @Override
                                                                              public void onDataChange(DataSnapshot dataSnapshot) {
                                                                                  UserPojo userPojo = dataSnapshot.getValue(UserPojo.class);
                                                                                  if (preferences.getString("NOTIFICATIONS DISMISSED", "").equals("dismissed")) {
                                                                                      notifications.clear();
                                                                                      senders.clear();
                                                                                  }
                                                                                  if (count == 1)
                                                                                      if(!preferences.getString("CHAT_PAGE_OPENED", "closed").equals(userPojo.getUserId())) {
                                                                                          MessageNotify(userPojo.getName(), message, userPojo.getUserId());
                                                                                      }

                                                                                          editor.putString("NOTIFICATIONS DISMISSED", "created");
                                                                                          editor.apply();




                                                                                  myRef.child(userId).child(dataSnapshot.getKey()).removeEventListener(this);
                                                                              }

                                                                              @Override
                                                                              public void onCancelled(DatabaseError databaseError) {

                                                                              }

                                                                          });
                                                                      }
                                                                      myRef.child(userId).child(dataSnapshot.getKey()).removeEventListener(this);
                                                                  }


                                                                  @Override
                                                                  public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                                                                  }

                                                                  @Override
                                                                  public void onChildRemoved(DataSnapshot dataSnapshot) {

                                                                  }

                                                                  @Override
                                                                  public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                                                                  }

                                                                  @Override
                                                                  public void onCancelled(DatabaseError databaseError) {

                                                                  }
                                                              });

                                                              myRef.child(userId).removeEventListener(this);

                                                          }

                                                          @Override
                                                          public void onChildRemoved(DataSnapshot dataSnapshot) {

                                                          }

                                                          @Override
                                                          public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                                                          }

                                                          @Override
                                                          public void onCancelled(DatabaseError error) {
                                                              // Failed to read value

                                                          }
                                                      }

            );
        }


     return START_STICKY;
    }


    public void onDestroy()
    {
        Toast.makeText(getBaseContext(),"Service Stopped",Toast.LENGTH_SHORT).show();
        count=0;
        super.onDestroy();

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void MessageNotify(String name, String message,String id)
    {
        NotificationCompat.Builder mBuilder =
                 new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_message_white_24dp)
                        .setContentTitle(name)
                        .setContentText(message)
                         .setDefaults(Notification.DEFAULT_SOUND)
                        .setDeleteIntent(createOnDismissedIntent(getBaseContext(), 1));


        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();


        //events[numMessages++] = new String(message);
        notifications.add(new String(name+" : "+message));
        senders.add(new String(id));

        // Sets a title for the Inbox style big view
        inboxStyle.setBigContentTitle("New Messages");

        // Moves events into the big view
        for (int i=0; i < notifications.size(); i++) {
            inboxStyle.addLine(notifications.get(i));
        }
        mBuilder.setStyle(inboxStyle);

        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // mId allows you to update the notification later on.
        Intent resultIntent;
        int number=0;
        if(senders.size()==1)
        {
            resultIntent = new Intent(this, ChatPage.class);
            resultIntent.putExtra(Constants.KEY_ACTIVE_CHAT, id);
        }
        else
        {
            for(int j=1; j< senders.size();j++)
            {
                if(!senders.get(j).equals(senders.get(0)))
                {
                    number++;
                    break;
                }
            }
            if(number==0)
            {
                resultIntent = new Intent(this, ChatPage.class);
                resultIntent.putExtra(Constants.KEY_ACTIVE_CHAT, senders.get(0));
            }
            else {

                resultIntent = new Intent(this, MainActivity.class);
            }
        }
    // Because clicking the notification opens a new ("special") activity, there's
    // no need to create an artificial back stack.
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        this,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        mBuilder.setContentIntent(resultPendingIntent);

        mNotificationManager.notify(1,mBuilder.build());
    }


    private PendingIntent createOnDismissedIntent(Context context, int notificationId) {
        Intent intent = new Intent(context, NotificationDismissedReceiver.class);
        intent.putExtra("com.my.app.notificationId", notificationId);


        PendingIntent pendingIntent =
                PendingIntent.getBroadcast(context.getApplicationContext(),
                        notificationId, intent, 0);
        return pendingIntent;
    }
}
