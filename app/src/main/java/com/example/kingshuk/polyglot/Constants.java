package com.example.kingshuk.polyglot;

/**
 * Created by Kingshuk on 28-Jun-16.
 */
public class Constants {
    public static final String KEY_ENCODED_EMAIL = "ENCODED_EMAIL";
    public static final String KEY_ACTIVE_LIST = "Active List";
    public static final String KEY_ACTIVE_CHAT= "Active Chat";
    public static final String USERS = "USERS";
    public static final String USER_DETAILS = "USER_DETAILS";
    public static final String CHAT = "CHAT";
    public static final String CHAT_DETAILS= "CHAT_DETAILS";
    public static final String PRESENCE= "PRESENCE";
    public static final String LAST_ONLINE= "LAST_ONLINE";
    public static final String CONNECTIONS= "CONNECTIONS";
    public static final String TYPING= "TYPING";
    public static final String MY_LIKES= "MY_LIKES";
    public static final String PROFILE_LIKES= "PROFILE_LIKES";
    public static final String FIREBASE_PROPERTY_TIMESTAMP = "timestamp";
    public static final String BLOCK= "BLOCK";


}
