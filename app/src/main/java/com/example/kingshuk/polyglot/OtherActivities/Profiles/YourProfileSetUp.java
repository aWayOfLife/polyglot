package com.example.kingshuk.polyglot.OtherActivities.Profiles;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kingshuk.polyglot.Constants;
import com.example.kingshuk.polyglot.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.mukesh.countrypicker.fragments.CountryPicker;
import com.mukesh.countrypicker.interfaces.CountryPickerListener;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class YourProfileSetUp extends AppCompatActivity {
    final String[] items = {"Spanish","English","French","Russian","Finnish","Portugese","German"};
    ImageView toYourProfile;
    TextView birthday;
    TextView male;
    TextView female;
    TextView country;
    ImageView flag;
    ImageView mUserProfilePic ;
    EditText mUsername;
    EditText mUserTag;
    EditText mUserIntroduction;
    LinearLayout mUserLanguageButton;
    TextView mUserLanguagesKnown;
    String path="";
    Uri outputFileUri;String tmppath;
    File sdImageMainDirectory;
    Calendar myCalendar;
    DatePickerDialog.OnDateSetListener date;
    String country_drawable="";
    String gender="";
    String p="B";
    String bd=""+System.currentTimeMillis()/1000;
    ArrayList<String> languagesKnown;
    String image="";
    String hugeImage="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_your_profile_set_up);
        languagesKnown=new ArrayList<String>();
        toYourProfile=(ImageView)findViewById(R.id.toYourProfile);
         birthday=(TextView) findViewById(R.id.birthday);
        male=(TextView)findViewById(R.id.male);
         female=(TextView)findViewById(R.id.female);
         country=(TextView)findViewById(R.id.country);
         flag=(ImageView)findViewById(R.id.flag);
         mUserProfilePic=(ImageView)findViewById(R.id.user_profile_pic) ;
        mUsername=(EditText)findViewById(R.id.user_name);
         mUserTag=(EditText)findViewById(R.id.user_tag);
         mUserIntroduction=(EditText)findViewById(R.id.user_introduction);
        mUserLanguageButton=(LinearLayout)findViewById(R.id.user_languages_button);
        mUserLanguagesKnown=(TextView)findViewById(R.id.user_languages_textview) ;
        mUserLanguageButton.setClickable(true);
        birthday.setClickable(true);
        male.setClickable(true);
        female.setClickable(true);
        country.setClickable(true);
        toYourProfile.setClickable(true);

        toYourProfile.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                final FirebaseUser use = FirebaseAuth.getInstance().getCurrentUser();
                final String userId = use.getUid();
                final FirebaseDatabase database = FirebaseDatabase.getInstance();
                final DatabaseReference myRef = database.getReference(Constants.USERS);

                if (path != "") {
                    FirebaseStorage storage = FirebaseStorage.getInstance();
                    StorageReference storageRef = storage.getReferenceFromUrl("gs://polyglot-2-126ec.appspot.com");
                    StorageReference smallProfileImageRef = storageRef.child("ProfilePic/" + userId + ".jpg");
                    StorageReference largeProfileImageRef = storageRef.child("ProfilePic/Large/" + userId + ".jpg");

                    Toast.makeText(YourProfileSetUp.this,path, Toast.LENGTH_LONG).show();


                    Bitmap smallBitmap = BitmapFactory.decodeFile(path);
                    smallBitmap = Bitmap.createScaledBitmap(smallBitmap,144,144,true);
                    ByteArrayOutputStream sbaos = new ByteArrayOutputStream();
                    smallBitmap.compress(Bitmap.CompressFormat.JPEG, 100, sbaos);
                    byte[] smallImage = sbaos.toByteArray();

                    Bitmap largeBitmap = BitmapFactory.decodeFile(path);
                    largeBitmap = Bitmap.createScaledBitmap(largeBitmap,350,350,true);
                    ByteArrayOutputStream lbaos = new ByteArrayOutputStream();
                    largeBitmap.compress(Bitmap.CompressFormat.JPEG, 100, lbaos);
                    final byte[] largeImage = lbaos.toByteArray();



                    UploadTask smallUploadTask = smallProfileImageRef.putBytes(smallImage);

                    smallUploadTask.addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            // Handle unsuccessful uploads
                        }
                    }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                            Uri downloadUrl = taskSnapshot.getDownloadUrl();
                            image=downloadUrl.toString();
                            UserPojo user = new UserPojo(mUsername.getText().toString(),bd, mUserTag.getText().toString(), mUserIntroduction.getText().toString(), use.getUid().toString(),gender, country_drawable,languagesKnown,image);
                            myRef.child(use.getUid().toString()).setValue(user);
                            path = "";
                        }
                    });

                    UploadTask bigUploadTask = largeProfileImageRef.putBytes(largeImage);

                    bigUploadTask.addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            // Handle unsuccessful uploads
                        }
                    }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                            Uri downloadUrl = taskSnapshot.getDownloadUrl();
                            hugeImage=downloadUrl.toString();
                            Intent intent = new Intent(YourProfileSetUp.this, YourProfile.class);
                            intent.putExtra("HUGE_IMAGE",hugeImage);
                            startActivity(intent);
                            overridePendingTransition(R.anim.enter, R.anim.exit);
                        }
                    });



                }
                else
                {
                    UserPojo user = new UserPojo(mUsername.getText().toString(),bd, mUserTag.getText().toString(), mUserIntroduction.getText().toString(), "null",gender, country_drawable,languagesKnown,"none");
                    myRef.child(use.getUid().toString()).setValue(user);
                    Intent intent = new Intent(YourProfileSetUp.this, YourProfile.class);
                    intent.putExtra("HUGE_IMAGE",hugeImage);
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                }




            }
        });
        male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gender="M";
                female.setTypeface(null,Typeface.NORMAL);
                male.setTypeface(null, Typeface.BOLD);
                male.setTextColor(Color.parseColor("#2196F3"));
                female.setTextColor(Color.parseColor("#98ffffff"));

            }
        });
        female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gender="F";
                male.setTypeface(null,Typeface.NORMAL);
                female.setTypeface(null, Typeface.BOLD);
                female.setTextColor(Color.parseColor("#F06292"));
                male.setTextColor(Color.parseColor("#98ffffff"));

            }
        });
        country.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                CountryPicker picker = CountryPicker.newInstance("Select Country");
                picker.show(getSupportFragmentManager(), "COUNTRY PICKER");
                picker.setListener(new CountryPickerListener() {

                    @Override
                    public void onSelectCountry(String name, String code, String dialCode, int flagDrawableResID) {
                        // Implement your code here
                        country.setText(name);
                        DialogFragment dialogFragment =
                                (DialogFragment) getSupportFragmentManager().findFragmentByTag("COUNTRY PICKER");
                        dialogFragment.dismiss();
                        country_drawable = "" + flagDrawableResID + "," + name;

                    }
                });

            }
        });

        mUserProfilePic.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setProfilePic();

            }
        });
       myCalendar = Calendar.getInstance();

       date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        birthday.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(YourProfileSetUp.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        mUserLanguageButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                final ArrayList seletedItems=new ArrayList();

                AlertDialog dialog = new AlertDialog.Builder(YourProfileSetUp.this)
                        .setTitle("What Do You Know?")
                        .setMultiChoiceItems(items, null, new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int indexSelected, boolean isChecked) {
                                if (isChecked) {
                                    // If the user checked the item, add it to the selected items
                                    //seletedItems.add(indexSelected);
                                    proficiency(indexSelected);
                                } else if (languagesKnown.contains(String.valueOf(items[indexSelected]+" B")) ||
                                        languagesKnown.contains(String.valueOf(items[indexSelected]+" I")) ||
                                        languagesKnown.contains(String.valueOf(items[indexSelected]+" F"))  ||
                                        languagesKnown.contains(String.valueOf(items[indexSelected]+" N"))) {
                                    // Else, if the item is already in the array, remove it
                                    //seletedItems.remove(Integer.valueOf(indexSelected));
                                    languagesKnown.remove(String.valueOf(items[indexSelected]+" B"));
                                    languagesKnown.remove(String.valueOf(items[indexSelected]+" I"));
                                    languagesKnown.remove(String.valueOf(items[indexSelected]+" F"));
                                    languagesKnown.remove(String.valueOf(items[indexSelected]+" N"));
                                }
                            }
                        }).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                String s="";
                               for(int i=0;i<languagesKnown.size();i++)
                                {
                                    s=s+languagesKnown.get(i).substring(0,languagesKnown.get(i).indexOf(" "))+"  ";
                                }
                                mUserLanguagesKnown.setText(s);
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                //  Your code when user clicked on Cancel
                            }
                        }).create();
                dialog.show();
            }
        });


    }
    private void updateLabel() {

        String myFormat = "dd/MM/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        birthday.setText(sdf.format(myCalendar.getTime()));
        bd=""+(myCalendar.getTimeInMillis()/1000);
    }

    void setProfilePic()
    {
        tmppath = Environment.getExternalStorageDirectory().getAbsolutePath();
        tmppath += "/Polyglot";
        File file = new File(tmppath);
        file.mkdirs();
        sdImageMainDirectory = new File(file, "myPicName");
        outputFileUri = Uri.fromFile(sdImageMainDirectory);
        Intent pickImageIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickImageIntent.setType("image/*");
        pickImageIntent.putExtra("scaleType", "centerCrop");
        pickImageIntent.putExtra("crop", "true");
        pickImageIntent.putExtra("aspectX", 1);
        pickImageIntent.putExtra("aspectY", 1);
        pickImageIntent.putExtra("outputX", 350);
        pickImageIntent.putExtra("outputY", 350);
        pickImageIntent.putExtra("scale", true);
        pickImageIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        pickImageIntent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());

        startActivityForResult(pickImageIntent, 2);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 2 ) {
                Bitmap bitmap=null;
                if(data.getData()==null){
                    bitmap = (Bitmap)data.getExtras().get("data");
                }
                else{
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), data.getData());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                try {
                    path=saveToInternalStorage(bitmap);
                    Picasso
                            .with(this)
                            .load("file://" + path)
                            .placeholder(mUserProfilePic.getDrawable())
                            .into(mUserProfilePic);
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }

            }
        }
    }


    private String saveToInternalStorage(Bitmap bitmapImage) throws IOException {
        ContextWrapper cw = new ContextWrapper(getApplicationContext());
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

        File mypath=new File(directory,"MyProfilePic.jpg");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            bitmapImage.compress(Bitmap.CompressFormat.JPEG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            fos.close();
        }
        String path= mypath.getAbsolutePath();
        return path;
    }

    void proficiency(final int index)
    {
        final CharSequence[] levels = {" Beginner "," Intermediate "," Fluent "," Native "};
        AlertDialog levelDialog;
        // Creating and Building the Dialog
        final AlertDialog.Builder builder = new AlertDialog.Builder(YourProfileSetUp.this);
        builder.setTitle("How Well Do You Know?");
        builder.setSingleChoiceItems(levels, 0, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {


                switch(item)
                {
                    case 0:
                        // Your code when first option seletced
                        p="B";
                        break;
                    case 1:
                        // Your code when 2nd  option seletced
                        p="I";
                        break;
                    case 2:
                        // Your code when 3rd option seletced
                        p="F";
                        break;
                    case 3:
                        // Your code when 4th  option seletced
                        p="N";
                        break;

                }

            }
        });
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id ) {
                languagesKnown.add(items[index]+" " + p);
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                Toast.makeText(YourProfileSetUp.this,"Not Selected",Toast.LENGTH_SHORT).show();
            }
        });

        levelDialog = builder.create();
        levelDialog.show();

    }
}
