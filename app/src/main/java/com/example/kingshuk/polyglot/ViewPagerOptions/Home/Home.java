package com.example.kingshuk.polyglot.ViewPagerOptions.Home;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.kingshuk.polyglot.Constants;
import com.example.kingshuk.polyglot.OtherActivities.Profiles.ProfileActivity;
import com.example.kingshuk.polyglot.OtherActivities.Profiles.UserPojo;
import com.example.kingshuk.polyglot.R;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;


public class Home extends Fragment {


    private List<UserPojo> homeList = new ArrayList<UserPojo>();
    private RecyclerView recyclerView;
    FirebaseRecyclerAdapter<UserPojo, HomeHolder> mRecyclerViewAdapter;
    public Home() {
        /* Required empty public constructor*/
    }


    // TODO: Rename and change types and number of parameters
    public static Home newInstance() {
        Home fragment = new Home();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View rootView =inflater.inflate(R.layout.fragment_home, container, false);

        prepareEntryData();

        recyclerView = (RecyclerView)rootView.findViewById(R.id.recycler_view_home);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(mRecyclerViewAdapter);
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        // TODO Handle item click
                        UserPojo selectedList = mRecyclerViewAdapter.getItem(position);
                        if (selectedList != null) {
                            SharedPreferences preferences;
                            SharedPreferences.Editor editor;
                            preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
                            editor = preferences.edit();
                            editor.putString("MAIN EXIT TYPE","Other Activity");
                            editor.apply();
                            Intent intent = new Intent(getContext(), ProfileActivity.class);
                            String listId = mRecyclerViewAdapter.getRef(position).getKey();
                            ImageView pic=(ImageView)view.findViewById(R.id.home_item_profile_pic);
                            pic.buildDrawingCache();
                            Bitmap bitmap= pic.getDrawingCache();
                            intent.putExtra("BitmapImage", bitmap);
                            intent.putExtra(Constants.KEY_ACTIVE_LIST, listId);
                            startActivity(intent);
                        }
                    }
                })
        );
        return rootView;
    }

    void prepareEntryData() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(Constants.USERS);
        myRef.keepSynced(true);

        mRecyclerViewAdapter = new FirebaseRecyclerAdapter<UserPojo, HomeHolder>(UserPojo.class, R.layout.items_home, HomeHolder.class, myRef) {
            @Override
            public void populateViewHolder(HomeHolder chatMessageViewHolder, UserPojo chatMessage, int position) {

                    chatMessageViewHolder.setName(chatMessage.getName());
                    chatMessageViewHolder.setAge(chatMessage.getAge());
                    chatMessageViewHolder.setTag(chatMessage.getTag());
                    chatMessageViewHolder.setGender(chatMessage.getGender());
                    chatMessageViewHolder.setCountry(chatMessage.getCountry());
                    chatMessageViewHolder.setIntroduction(chatMessage.getIntroduction());
                    chatMessageViewHolder.setLanguages(chatMessage.getLanguages());
                    chatMessageViewHolder.setImage(chatMessage.getImage());
                    if (chatMessage.getUserId().equalsIgnoreCase("null") == false)
                        chatMessageViewHolder.setUserId(chatMessage.getUserId());



                }
            }

            ;

    }

    @Override
    public void onPause() {
        super.onPause();
    }


}

