package com.example.kingshuk.polyglot;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Kingshuk on 28-Jun-16.
 */
import android.app.ProgressDialog;

public class BaseActivity extends AppCompatActivity {

    private ProgressDialog mProgressDialog;
    protected String mProvider, mEncodedEmail;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }



    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }


    @Override
    public void onStop() {
        super.onStop();
        hideProgressDialog();
    }

}
