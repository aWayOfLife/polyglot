package com.example.kingshuk.polyglot.ViewPagerOptions.Message;

/**
 * Created by SAPTARSHI DE on 05-07-2016.
 */
public class ChatPojo {
    String name, userId;

    public ChatPojo() {

    }

    public ChatPojo(String name, String userId) {
        this.name = name;
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public String getUserId() {
        return userId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
