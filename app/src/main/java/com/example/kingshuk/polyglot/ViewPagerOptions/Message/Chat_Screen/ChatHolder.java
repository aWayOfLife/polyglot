package com.example.kingshuk.polyglot.ViewPagerOptions.Message.Chat_Screen;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.kingshuk.polyglot.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

/**
 * Created by SAPTARSHI DE on 05-07-2016.
 */
public class ChatHolder extends RecyclerView.ViewHolder {
    View mView;
    Context mContext;
    FirebaseUser use = FirebaseAuth.getInstance().getCurrentUser();
    String me = use.getUid();

    public ChatHolder(View itemView) {
        super(itemView);
        mView = itemView;
    }

    public void setMessage(String message) {
        TextView field = (TextView) mView.findViewById(R.id.message);
        field.setText(message);
    }

    public void setOwner(String userId) {
        TextView field = (TextView) mView.findViewById(R.id.message);
        mContext = mView.getContext();

        LinearLayout linearLayout = (LinearLayout) mView.findViewById(R.id.messageLinearLayout);
        int l, r;
        Resources resources = mContext.getResources();
        if (me.equals(userId)) {

            l = (int) TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP, 65, resources
                            .getDisplayMetrics());
            r = (int) TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP, 0, resources
                            .getDisplayMetrics());

            linearLayout.setGravity(Gravity.END);
        } else {
            r = (int) TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP, 65, resources
                            .getDisplayMetrics());
            l = (int) TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP, 0, resources
                            .getDisplayMetrics());

            linearLayout.setGravity(Gravity.START);
        }

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(l, 0, r, 0);
        field.setLayoutParams(params);
        Typeface typeface = Typeface.createFromAsset(mContext.getAssets(), "DancingScript-Bold.ttf");
        field.setTypeface(typeface);
    }
}
