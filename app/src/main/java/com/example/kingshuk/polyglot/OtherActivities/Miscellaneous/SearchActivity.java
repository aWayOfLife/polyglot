package com.example.kingshuk.polyglot.OtherActivities.Miscellaneous;

import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.preference.PreferenceManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.kingshuk.polyglot.Constants;
import com.example.kingshuk.polyglot.OtherActivities.Profiles.ProfileActivity;
import com.example.kingshuk.polyglot.OtherActivities.Profiles.UserPojo;
import com.example.kingshuk.polyglot.R;
import com.example.kingshuk.polyglot.ViewPagerOptions.Home.Home;
import com.example.kingshuk.polyglot.ViewPagerOptions.Home.HomeHolder;
import com.example.kingshuk.polyglot.ViewPagerOptions.Home.RecyclerItemClickListener;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class SearchActivity extends AppCompatActivity {
    String query;
    private List<UserPojo> homeList = new ArrayList<UserPojo>();
    private RecyclerView recyclerView;
    FirebaseRecyclerAdapter<UserPojo, SearchHolder> mRecyclerViewAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_search);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        handleIntent(getIntent());

    }

    @Override
    protected void onStart() {
        super.onStart();
        recyclerView = (RecyclerView)findViewById(R.id.recycler_view_search);
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(SearchActivity.this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        // TODO Handle item click
                        UserPojo selectedList = mRecyclerViewAdapter.getItem(position);
                        if (selectedList != null) {
                            SharedPreferences preferences;
                            SharedPreferences.Editor editor;
                            preferences = PreferenceManager.getDefaultSharedPreferences(SearchActivity.this);
                            editor = preferences.edit();
                            editor.putString("MAIN EXIT TYPE","Other Activity");
                            editor.apply();
                            Intent intent = new Intent(SearchActivity.this, ProfileActivity.class);
                            String listId = mRecyclerViewAdapter.getRef(position).getKey();
                            ImageView pic=(ImageView)view.findViewById(R.id.home_item_profile_pic);
                            pic.buildDrawingCache();
                            Bitmap bitmap= pic.getDrawingCache();
                            intent.putExtra("BitmapImage", bitmap);
                            intent.putExtra(Constants.KEY_ACTIVE_LIST, listId);
                            startActivity(intent);
                        }
                    }
                })
        );

    }


    void prepareEntryData(final String q) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(Constants.USERS);

        mRecyclerViewAdapter = new FirebaseRecyclerAdapter<UserPojo, SearchHolder>(UserPojo.class, R.layout.items_home, SearchHolder.class, myRef) {
            @Override
            public void populateViewHolder(SearchHolder chatMessageViewHolder, UserPojo chatMessage, int position) {

                chatMessageViewHolder.setName(chatMessage.getName(),q);
                chatMessageViewHolder.setAge(chatMessage.getAge());
                chatMessageViewHolder.setTag(chatMessage.getTag());
                chatMessageViewHolder.setGender(chatMessage.getGender());
                chatMessageViewHolder.setCountry(chatMessage.getCountry());
                chatMessageViewHolder.setIntroduction(chatMessage.getIntroduction());
                chatMessageViewHolder.setLanguages(chatMessage.getLanguages());
                if (chatMessage.getUserId().equalsIgnoreCase("null") == false)
                    chatMessageViewHolder.setUserId(chatMessage.getUserId());


            }
        }

        ;

    }




    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        handleIntent(intent);
    }


    private void handleIntent(Intent intent) {

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            query = intent.getStringExtra(SearchManager.QUERY);
            //use the query to search your data somehow
            Toast.makeText(SearchActivity.this, query, Toast.LENGTH_LONG).show();
            prepareEntryData(query);
            recyclerView = (RecyclerView)findViewById(R.id.recycler_view_search);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SearchActivity.this);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setAdapter(mRecyclerViewAdapter);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search, menu);
        for (int i = 0; i < menu.size(); i++) {
            Drawable drawable = menu.getItem(i).getIcon();
            if (drawable != null) {
                drawable.mutate();
                drawable.setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
            }
        }


        if (menu.findItem(R.id.action_search).isVisible() == true) {
            SearchManager searchManager =
                    (SearchManager) getSystemService(Context.SEARCH_SERVICE);
            SearchView searchView =
                    (SearchView) menu.findItem(R.id.action_search).getActionView();

            //searchView.setQuery(query, false);
            searchView.setIconified(false);
            //searchView.clearFocus();
            searchView.setQueryHint("Search");
            menu.findItem(R.id.action_search).expandActionView();
            ComponentName cn = new ComponentName(this, SearchActivity.class);
            searchView.setSearchableInfo(searchManager.getSearchableInfo(cn));

        }
        return true;

    }
}
