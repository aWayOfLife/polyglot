package com.example.kingshuk.polyglot.ViewPagerOptions.Account;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kingshuk.polyglot.Constants;
import com.example.kingshuk.polyglot.MainActivity;
import com.example.kingshuk.polyglot.OtherActivities.Login_Signup.LoginActivity;
import com.example.kingshuk.polyglot.OtherActivities.Profiles.ProfileActivity;
import com.example.kingshuk.polyglot.OtherActivities.Profiles.UserPojo;
import com.example.kingshuk.polyglot.R;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Calendar;


public class Account extends Fragment {

    LinearLayout myProfile,logout,mLanguages;
    ImageView mProfilePic;
    TextView mAccountName;

    FirebaseDatabase database;
    FirebaseUser use;
    DatabaseReference lastOnlineRef;
    DatabaseReference connectedRef;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    String p="B";
    ArrayList<String> languagesKnown;
    UserPojo userPojo;
    DatabaseReference myRef;
    final String[] items = {"Spanish","English","French","Russian","Finnish","Portugese","German"};
    public Account() {
        /* Required empty public constructor*/
    }


    // TODO: Rename and change types and number of parameters
    public static Account newInstance() {
        Account fragment = new Account();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_account, container, false);
        mAccountName=(TextView)rootView.findViewById(R.id.account_name);
        myProfile=(LinearLayout)rootView.findViewById(R.id.button_profile);
        logout=(LinearLayout) rootView.findViewById(R.id.button_logout);
        mProfilePic=(ImageView)rootView.findViewById(R.id.account_ProfilePic);
        mLanguages=(LinearLayout)rootView.findViewById(R.id.account_languages);
        mLanguages.setClickable(true);
        myProfile.setClickable(true);
        logout.setClickable(true);
        preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        editor = preferences.edit();

        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        use = FirebaseAuth.getInstance().getCurrentUser();
        lastOnlineRef = database.getReference(Constants.LAST_ONLINE).child(use.getUid());
        connectedRef = database.getReference(Constants.CONNECTIONS).child(use.getUid());

        connectedRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                boolean connected = snapshot.getValue(Boolean.class);
                if (connected) {
                    // add this device to my connections list
                    // this value could contain info about the device or a timestamp too
                    connectedRef.setValue(Boolean.TRUE);

                    // when this device disconnects, remove it
                    connectedRef.onDisconnect().setValue(Boolean.FALSE);

                    // when I disconnect, update the last time I was seen online
                    lastOnlineRef.onDisconnect().setValue(ServerValue.TIMESTAMP);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }


        });




        String userId = use.getUid();

        myRef = database.getReference(Constants.USERS).child(userId);
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                userPojo = dataSnapshot.getValue(UserPojo.class);
                languagesKnown=userPojo.getLanguages();
                mAccountName.setText(userPojo.getName());

                Picasso.with(getContext()).load(Uri.parse(userPojo.getImage())).networkPolicy(NetworkPolicy.OFFLINE)
                        //.resize(field.getHeight(), field.getWidth())
                        .placeholder(mProfilePic.getDrawable())
                        //.centerCrop()
                        .into(mProfilePic, new Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError() {
                                Picasso.with(getContext()).load(Uri.parse(userPojo.getImage())).resize(mProfilePic.getHeight(), mProfilePic.getWidth()).placeholder(mProfilePic.getDrawable()).error(mProfilePic.getDrawable()).centerCrop().into(mProfilePic);
                            }
                        });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



        logout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                editor.putString("STATE", "Signed Out");
                editor.apply();
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(getActivity(),LoginActivity.class);
                startActivity(intent);
            }
        });

        myProfile.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

            }
        });

        mLanguages.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                final ArrayList seletedItems=new ArrayList();
                boolean[] checked={false,false,false,false,false,false,false};

                for(int i=0;i<items.length;i++)
                {
                    for(int j=0;j<languagesKnown.size();j++)
                    {
                        if(items[i].equals(languagesKnown.get(j).substring(0,languagesKnown.get(j).indexOf(" "))))
                        {
                            checked[i]=true;
                        }
                    }
                }
                AlertDialog dialog = new AlertDialog.Builder(getContext())
                        .setTitle("What Do You Know?")
                        .setMultiChoiceItems(items, checked, new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int indexSelected, boolean isChecked) {
                                if (isChecked) {
                                    // If the user checked the item, add it to the selected items
                                    //seletedItems.add(indexSelected);
                                    proficiency(indexSelected);

                                } else if (languagesKnown.contains(String.valueOf(items[indexSelected]+" B")) ||
                                        languagesKnown.contains(String.valueOf(items[indexSelected]+" I")) ||
                                languagesKnown.contains(String.valueOf(items[indexSelected]+" F"))  ||
                                languagesKnown.contains(String.valueOf(items[indexSelected]+" N"))) {
                                    // Else, if the item is already in the array, remove it
                                    //seletedItems.remove(Integer.valueOf(indexSelected));
                                    languagesKnown.remove(String.valueOf(items[indexSelected]+" B"));
                                    languagesKnown.remove(String.valueOf(items[indexSelected]+" I"));
                                    languagesKnown.remove(String.valueOf(items[indexSelected]+" F"));
                                    languagesKnown.remove(String.valueOf(items[indexSelected]+" N"));
                                }
                            }
                        }).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                UserPojo updatedPojo= userPojo;
                                myRef.setValue(updatedPojo);
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                //  Your code when user clicked on Cancel
                            }
                        }).create();
                dialog.show();
            }

        });

        return rootView;
    }

    void proficiency(final int index)
    {

        final CharSequence[] levels = {" Beginner "," Intermediate "," Fluent "," Native "};
        AlertDialog levelDialog;
        // Creating and Building the Dialog
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("How Well Do You Know?");
        builder.setSingleChoiceItems(levels, 0, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {


                switch(item)
                {
                    case 0:
                        // Your code when first option seletced
                        p="B";

                        break;
                    case 1:
                        // Your code when 2nd  option seletced
                        p="I";

                        break;
                    case 2:
                        // Your code when 3rd option seletced

                        p="F";

                        break;
                    case 3:
                        // Your code when 4th  option seletced

                        p="N";

                        break;


                }

            }
        });
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id ) {
                languagesKnown.add(items[index]+" " + p);
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                Toast.makeText(getContext(),"Not Selected",Toast.LENGTH_SHORT).show();
            }
        });

        levelDialog = builder.create();
        levelDialog.show();
    }

    @Override
    public void onPause() {
        super.onPause();
    }


}
