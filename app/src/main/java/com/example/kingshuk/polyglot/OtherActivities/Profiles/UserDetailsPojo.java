package com.example.kingshuk.polyglot.OtherActivities.Profiles;

import java.util.List;

/**
 * Created by Kingshuk on 29-Jun-16.
 */
public class UserDetailsPojo {

    String moreInfo,movies,songs,books,hugeImage;
    List<String> interests;

    public UserDetailsPojo() {
    }
    public UserDetailsPojo(String moreInfo,String movies,String songs,String books,List<String> interests,String hugeImage){
        this.moreInfo=moreInfo;
        this.movies=movies;
        this.songs=songs;
        this.books=books;
        this.interests=interests;
        this.hugeImage=hugeImage;
    }
    public String getMoreInfo(){
        return moreInfo;
    }
    public String getMovies(){
        return movies;
    }
    public String getSongs(){
        return songs;

    }

    public String getHugeImage() {
        return hugeImage;
    }

    public void setHugeImage(String hugeImage) {
        this.hugeImage = hugeImage;
    }

    public String getBooks(){
        return books;
    }
    public List<String> getInterests() {
        return interests;
    }

    public void setInterests(List<String> interests) {
        this.interests = interests;
    }

    public void setMoreInfo(String moreInfo){
        this.moreInfo=moreInfo;
    }
    public void setMovies(String movies){
        this.movies=movies;
    }
    public void setSongs(String songs){
        this.songs=songs;
    }
    public void setBooks(String books){
        this.books=books;
    }


    }

