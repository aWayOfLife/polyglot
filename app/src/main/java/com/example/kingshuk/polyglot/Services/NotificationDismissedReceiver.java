package com.example.kingshuk.polyglot.Services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.widget.Toast;


public class NotificationDismissedReceiver extends BroadcastReceiver {
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    @Override
    public void onReceive(Context context, Intent intent) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        editor = preferences.edit();
        editor.putString("NOTIFICATIONS DISMISSED","dismissed");
        editor.apply();
        int notificationId = intent.getExtras().getInt("com.my.app.notificationId");
      /* Your code to handle the event here */
        Toast.makeText(context,"Dismissed",Toast.LENGTH_SHORT).show();

    }
}