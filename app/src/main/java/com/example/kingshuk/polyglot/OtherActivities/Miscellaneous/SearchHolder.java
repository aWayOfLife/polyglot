package com.example.kingshuk.polyglot.OtherActivities.Miscellaneous;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.kingshuk.polyglot.Constants;
import com.example.kingshuk.polyglot.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Calendar;

import me.gujun.android.taggroup.TagGroup;

/**
 * Created by Kingshuk on 02-Jul-16.
 */
public class SearchHolder extends RecyclerView.ViewHolder {
    View mView;
    Context mContext;
    FirebaseStorage storage = FirebaseStorage.getInstance();
    StorageReference storageRef = storage.getReferenceFromUrl("gs://polyglot-2-126ec.appspot.com");
    boolean gone=false;

    public SearchHolder(View itemView) {
        super(itemView);
        mView = itemView;
    }

    public void setName(String name,String query) {
        if(name.toLowerCase().startsWith(query.toLowerCase())|| name.equalsIgnoreCase(query) || name.toLowerCase().contains(query.toLowerCase())) {
            TextView field = (TextView) mView.findViewById(R.id.home_item_name);
            field.setText(name);
        }
        else
        {
            gone=true;
            mView.findViewById(R.id.layout_to_hide).setVisibility(View.GONE);
        }
    }

    public void setAge(String text) {


            TextView field = (TextView) mView.findViewById(R.id.home_item_age);
            Long r = Long.parseLong(text) * 1000;
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(r);
            Calendar calendar1 = Calendar.getInstance();
            int age1 = 0;
            String age = "" + (calendar1.get(Calendar.YEAR) - calendar.get(Calendar.YEAR));
            if (calendar1.get(Calendar.YEAR) != calendar.get(Calendar.YEAR)) {
                if ((calendar1.get(Calendar.MONTH)) < ((calendar.get(Calendar.MONTH)))) {
                    age1 = Integer.parseInt(age);
                    age = "" + (--age1);
                } else if ((calendar1.get(Calendar.MONTH)) == ((calendar.get(Calendar.MONTH))))
                    if ((calendar1.get(Calendar.DAY_OF_MONTH)) < ((calendar.get(Calendar.DAY_OF_MONTH)))) {
                        age1 = Integer.parseInt(age);
                        age = "" + (--age1);
                    }

                field.setText(age);
            }


    }

    public void setTag(String text) {

            TextView field = (TextView) mView.findViewById(R.id.home_item_tag);
            field.setText(text);

    }

    public void setIntroduction(String text) {
        TextView field = (TextView) mView.findViewById(R.id.home_item_introduction);


        field.setText(text);
    }

    Context context;

    public void setUserId(String text) {

        if(gone!=true) {
            final ImageView field = (ImageView) mView.findViewById(R.id.home_item_profile_pic);
            //Picasso.with(mView.getContext()).load(R.drawable.p1).into(chatProfilePic);
            FirebaseStorage storage = FirebaseStorage.getInstance();
            StorageReference storageRef = storage.getReferenceFromUrl("gs://polyglot-2-126ec.appspot.com");
            storageRef.child("ProfilePic/" + text + ".jpg").getDownloadUrl().addOnSuccessListener(
                    new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            Picasso.with(mView.getContext()).load(uri).resize(field.getHeight(), field.getWidth()).placeholder(field.getDrawable()).error(field.getDrawable()).centerCrop().into(field);
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle any errors
                }
            });

            final TextView likes = (TextView) mView.findViewById(R.id.home_likes);
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference profileLikes = database.getReference(Constants.PROFILE_LIKES).child(text);
            profileLikes.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.getValue() == null || (long) dataSnapshot.getValue() == 0) {
                        likes.setText("0+");
                    } else {
                        likes.setText((long) dataSnapshot.getValue() + "+");
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

    }

    public void setGender(String text) {
        ImageView imageView = (ImageView) mView.findViewById(R.id.gender_display);
        if (text.equals("M")) {
            imageView.setImageResource(R.drawable.ic_gender_male_white_18dp);
            imageView.setColorFilter(Color.parseColor("#2196F3"));
        } else {
            imageView.setImageResource(R.drawable.ic_gender_female_white_24dp);
            imageView.setColorFilter(Color.parseColor("#F06292"));
        }

    }

    public void setCountry(String text) {
        ImageView imageView = (ImageView) mView.findViewById(R.id.country_display);
        String flag = text.substring(0, text.lastIndexOf(","));
        imageView.setImageResource(Integer.parseInt(flag));
    }

    public void setLanguages(ArrayList<String> languages) {
        TagGroup langStrong =(TagGroup)mView.findViewById(R.id.tag_languages_strong);
        TagGroup langWeak =(TagGroup)mView.findViewById(R.id.tag_languages_weak);
        ArrayList<String> strong=new ArrayList<String>();
        ArrayList<String> weak=new ArrayList<String>();
        for (int i=0;i<languages.size();i++)
        {
            int s=languages.get(i).indexOf(" ");
            char c=languages.get(i).charAt(s+1);
            if(c=='F' || c=='N')
            {
                strong.add(languages.get(i).substring(0,s));
            }
            else
            {
                weak.add(languages.get(i).substring(0,s));
            }
        }

        langStrong.setTags(strong);
        langWeak.setTags(weak);
    }
}