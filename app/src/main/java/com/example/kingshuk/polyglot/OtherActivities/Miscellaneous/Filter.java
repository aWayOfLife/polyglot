package com.example.kingshuk.polyglot.OtherActivities.Miscellaneous;


import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CheckBox;

import com.appyvet.rangebar.RangeBar;
import com.example.kingshuk.polyglot.MainActivity;
import com.example.kingshuk.polyglot.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;


public class Filter extends AppCompatActivity {
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    CheckBox checkBoxMale,checkBoxFemale,asia,africa,europe,northAmerica,southAmerica,oceania;
    RangeBar rangeBar;
    String minAge,maxAge;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_filter);
        Toolbar toolbar = (Toolbar) findViewById(R.id.filter_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Filter");

        minAge="0";
        maxAge="96";
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = preferences.edit();
        String gender = preferences.getString("FILTER_GENDER", "");
        checkBoxMale=(CheckBox)findViewById(R.id.male_filter);
        checkBoxFemale=(CheckBox)findViewById(R.id.female_filter);
        rangeBar = (RangeBar)findViewById(R.id.rangebar);

            if (gender.equalsIgnoreCase("M"))
                checkBoxMale.setChecked(true);
            else if (gender.equalsIgnoreCase("F"))
                checkBoxFemale.setChecked(true);

            else
            {
                checkBoxMale.setChecked(true);
                checkBoxFemale.setChecked(true);
            }
        String min_age = preferences.getString("FILTER_MINAGE","0");
        String max_age = preferences.getString("FILTER_MAXAGE","96");

        rangeBar.setRangePinsByIndices(Integer.parseInt(min_age),Integer.parseInt(max_age));


        rangeBar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex,
                                              int rightPinIndex,
                                              String leftPinValue, String rightPinValue) {
                if(Integer.parseInt(leftPinValue)>=0 && Integer.parseInt(leftPinValue)<=96 )
                minAge=leftPinValue;
                maxAge=rightPinValue;
            }
        });

        /*boolean created = preferences.getBoolean("DATABASE_GENERATED", false);
        if(created==false) {
            CountryManager mCountryManager = new CountryManager();
            mCountryManager.GenerateCountryList(Filter.this);
            editor.putBoolean("DATABASE_GENERATED", true);

        }*/
        //exportDB();
        asia=(CheckBox)findViewById(R.id.asia);
        africa=(CheckBox)findViewById(R.id.africa);
        europe=(CheckBox)findViewById(R.id.europe);
        northAmerica=(CheckBox)findViewById(R.id.nort_america);
        southAmerica=(CheckBox)findViewById(R.id.south_america);
        oceania=(CheckBox)findViewById(R.id.oceania);

        String zones = preferences.getString("FILTER_ZONES", "Asia.Africa.Europe.North America.South America.Oceania");
        String[] selectedZones = zones.split("\\.");
        for(int i=0;i<selectedZones.length;i++)
        {
            if(selectedZones[i].equals("Asia"))
            {
                asia.setChecked(true);
            }
            if(selectedZones[i].equals("Africa"))
            {
                africa.setChecked(true);
            }
            if(selectedZones[i].equals("Europe"))
            {
                europe.setChecked(true);
            }
            if(selectedZones[i].equals("North America"))
            {
                northAmerica.setChecked(true);
            }
            if(selectedZones[i].equals("South America"))
            {
                southAmerica.setChecked(true);
            }
            if(selectedZones[i].equals("Oceania"))
            {
                oceania.setChecked(true);
            }
        }






    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        checkBoxMale=(CheckBox)findViewById(R.id.male_filter);
        checkBoxFemale=(CheckBox)findViewById(R.id.female_filter);

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_save) {
            if(checkBoxMale.isChecked()&&checkBoxFemale.isChecked())
            {
                editor.putString("FILTER_GENDER", "ALL");
                editor.apply();
            }

            else if(checkBoxFemale.isChecked()) {
                editor.putString("FILTER_GENDER", "F");
            editor.apply();
            }
            else if(checkBoxMale.isChecked()) {
                editor.putString("FILTER_GENDER", "M");
                editor.apply();
            }


            editor.putString("FILTER_MINAGE", minAge);
            editor.apply();
            editor.putString("FILTER_MAXAGE", maxAge);
            editor.apply();

            String z="";

            if(asia.isChecked())
            {

                z=z+"Asia.";
            }
            if(africa.isChecked())
            {

                z=z+"Africa.";
            }
            if(europe.isChecked())
            {

                z=z+"Europe.";
            }
            if(northAmerica.isChecked())
            {

                z=z+"North America.";
            }
            if(southAmerica.isChecked())
            {

                z=z+"South America.";
            }
            if(oceania.isChecked())
            {

                z=z+"Oceania.";
            }

            editor.putString("FILTER_ZONES",z);
            editor.apply();

            Intent intent = new Intent(Filter.this, MainActivity.class);
            startActivity(intent);

            return true;

        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_filter, menu);
        for(int i = 0; i < menu.size(); i++){
            Drawable drawable = menu.getItem(i).getIcon();
            if(drawable != null) {
                drawable.mutate();
                drawable.setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
            }
        }

        return true;
    }
}
